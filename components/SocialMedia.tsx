import React, { Component } from 'react';
import { Image, Linking, TouchableOpacity, View } from 'react-native';
import linkedin from '../assets/linkedin.png';
import twitter from '../assets/twitter.png';
import instagram from '../assets/instagram.png';

class SocialMedia extends Component {
	state = {
		socialMediaList: [
			{
				name: 'Linkedin',
				icon: linkedin,
				url: 'https://www.linkedin.com/in/ridhoadtyaa/',
			},
			{
				name: 'twitter',
				icon: twitter,
				url: 'https://twitter.com/ridhoadtyaa',
			},
			{
				name: 'instagram',
				icon: instagram,
				url: 'https://www.instagram.com/ridhoadtyaa',
			},
		],
	};

	render() {
		return (
			<View style={{ flexDirection: 'row', marginTop: 30 }}>
				{this.state.socialMediaList.map(social => (
					<TouchableOpacity key={social.name} onPress={() => Linking.openURL(social.url)}>
						<Image source={social.icon} style={{ width: 20, height: 20, marginRight: 17 }} />
					</TouchableOpacity>
				))}
			</View>
		);
	}
}

export default SocialMedia;
